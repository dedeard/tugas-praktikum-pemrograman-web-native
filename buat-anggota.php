<?php
require_once './core/init.php';

if (!$admin->isLoggedIn()) {
  Redirect::to('login.php');
}

include_once "./includes/top.php";
include_once "./includes/nav.php";
?>
<div class="container py-3">
  <div class="card">
    <div class="card-header">Buat anggota</div>
    <form method="POST" class="card-body">
      <?php
      if (Input::exists()) {

        $validate = new Validate();
        $validation = $validate->check($_POST, [
          'nama' => ['required' => true, 'min' => 3, 'max' => 20],
          'no_hp' => ['required' => true, 'min' => 8, 'max' => 16]
        ]);

        if ($validation->passed()) {
          $nama = Input::get('nama');
          $no_hp = Input::get('no_hp');
          if (DB::getInstance()->query("INSERT INTO anggota (nama, no_hp) VALUES (?, ?)", [$nama, $no_hp])) {
            Redirect::to('anggota.php');
          } else {
            echo "<div class='alert alert-danger'>Gagal membuat anggota</div>";
          }
        } else {
          echo "<div class='alert alert-danger'><ul class='mb-0'>";
          foreach ($validation->errors() as $error) {
            echo "<li>$error</li>";
          }
          echo "</ul></div>";
        }
      }
      ?>
      <div class="form-group mb-3">
        <label for="nama">Nama</label>
        <input type="text" id="nama" name="nama" value="<?= Input::get('nama') ?>" class="form-control" />
      </div>
      <div class="form-group mb-3">
        <label for="no_hp">Nomor HP</label>
        <input type="text" id="no_hp" name="no_hp" value="<?= Input::get('no_hp') ?>" class="form-control" />
      </div>
      <div class="form-group mb-3">
        <button class="btn btn-primary px-5">Buat anggota</button>
      </div>
    </form>
  </div>
</div>

<?php include_once "./includes/bottom.php" ?>