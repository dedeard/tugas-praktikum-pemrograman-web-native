<?php

class Admin
{
  private $_db = null,
    $_data = null,
    $_sessionName = 'auth',
    $_isLoggedIn = false;

  public function __construct()
  {
    $this->_db = DB::getInstance();
    if (Session::exists($this->_sessionName)) {
      if ($this->find(Session::get($this->_sessionName))) {
        $this->_isLoggedIn = true;
      } else {
        $this->logout();
      }
    }
  }

  public function find($identity = null)
  {
    if ($identity) {
      $field = (is_numeric($identity)) ? 'id_admin' : 'username';
      $data = $this->_db->query("SELECT * FROM admin WHERE $field=?", [$identity]);
      if ($data->count()) {
        $this->_data = $data->first();
        return true;
      }
    }
    return false;
  }

  public function login($username = null, $password = null)
  {
    if ($this->find($username)) {
      if ($this->data()->password === md5($password)) {
        Session::put($this->_sessionName, $this->data()->id_admin);
        $this->_isLoggedIn = true;
        return true;
      }
    }
    return false;
  }

  public function logout()
  {
    Session::delete($this->_sessionName);
    $this->_data = null;
    $this->_isLoggedIn = false;
  }

  public function data()
  {
    return $this->_data;
  }

  public function isLoggedIn()
  {
    return $this->_isLoggedIn;
  }
}
