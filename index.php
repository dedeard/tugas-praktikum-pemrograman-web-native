<?php
require_once './core/init.php';

if (!$admin->isLoggedIn()) {
  Redirect::to('login.php');
}

include_once "./includes/top.php";
include_once "./includes/nav.php";
?>

<div class="container py-3">
  <div class="row">
    <div class="col-lg-4 mb-3">
      <div class="card card-body text-center py-5">
        <span class="block h1"><?= DB::getInstance()->query('SELECT * FROM buku')->count() ?></span>
        TOTAL BUKU
      </div>
    </div>
    <div class="col-lg-4 mb-3">
      <div class="card card-body text-center py-5">
        <span class="block h1"><?= DB::getInstance()->query('SELECT * FROM anggota')->count() ?></span>
        TOTAL ANGGOTA
      </div>
    </div>
    <div class="col-lg-4 mb-3">
      <div class="card card-body text-center py-5">
        <span class="block h1"><?= DB::getInstance()->query('SELECT * FROM pinjam')->count() ?></span>
        TOTAL PINJAMAN
      </div>
    </div>
  </div>
</div>

<?php include_once "./includes/bottom.php" ?>