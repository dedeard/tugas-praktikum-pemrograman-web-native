<nav class="navbar navbar-expand-lg bg-white shadow-sm py-3">
  <div class="container">
    <a class="navbar-brand" href="/">PERPUSTAKAAN</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link text-dark" href="/">Dasbor</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-dark" href="/anggota.php">Anggota</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-dark" href="/buku.php">Buku</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-dark" href="/peminjaman.php">Peminjaman</a>
        </li>
      </ul>
      <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="btn btn-danger" href="logout.php"><span class="text-capitalize"><?= $admin->data()->nama ?></span>, Logout</a>
        </li>
      </ul>
    </div>
  </div>
</nav>