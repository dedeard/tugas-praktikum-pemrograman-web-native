<?php
require_once './core/init.php';

if (!$admin->isLoggedIn()) {
  Redirect::to('login.php');
}

include_once "./includes/top.php";
include_once "./includes/nav.php";

$databuku = DB::getInstance()->query("SELECT * FROM buku")->results();
$dataanggota = DB::getInstance()->query("SELECT * FROM anggota")->results();

?>
<div class="container py-3">
  <div class="card">
    <div class="card-header">Tambah data peminjaman buku</div>
    <form method="POST" class="card-body">
      <?php
      if (Input::exists()) {

        $validate = new Validate();
        $validation = $validate->check($_POST, [
          'id_buku' => ['required' => true],
          'id_anggota' => ['required' => true]
        ]);
        if ($validation->passed()) {
          $id_anggota = Input::get('id_anggota');
          $id_buku = Input::get('id_buku');
          if (DB::getInstance()->query("INSERT INTO pinjam (id_anggota, id_buku) VALUES (?, ?)", [$id_anggota, $id_buku])) {
            Redirect::to('peminjaman.php');
          } else {
            echo "<div class='alert alert-danger'>Gagal membuat anggota</div>";
          }
        } else {
          echo "<div class='alert alert-danger'><ul class='mb-0'>";
          foreach ($validation->errors() as $error) {
            echo "<li>$error</li>";
          }
          echo "</ul></div>";
        }
      }
      ?>
      <div class="form-group mb-3">
        <label for="id_anggota">Anggota</label>
        <select name="id_anggota" id="id_anggota" class="form-control">
          <?php
          foreach (DB::getInstance()->query("SELECT * FROM anggota")->results() as $a) {
            $selected = Input::get('id_anggota') === $a->id_anggota ? "selected" : "";
            echo "<option value='$a->id_anggota' $selected>$a->nama, $a->no_hp</option>";
          }
          ?>
        </select>
      </div>
      <div class="form-group mb-3">
        <label for="id_buku">Buku</label>
        <select name="id_buku" id="id_buku" class="form-control">
          <?php
          foreach (DB::getInstance()->query("SELECT * FROM buku")->results() as $buku) {
            $selected = Input::get('id_buku') === $buku->id_buku ? "selected" : "";
            echo "<option value='$buku->id_buku' $selected>$buku->judul, by $buku->penulis</option>";
          }
          ?>
        </select>
      </div>
      <div class="form-group mb-3">
        <button class="btn btn-primary px-5">Tambah data</button>
      </div>
    </form>
  </div>
</div>

<?php include_once "./includes/bottom.php" ?>