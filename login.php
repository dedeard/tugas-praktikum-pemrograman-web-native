<?php
require_once "./includes/top.php";
require_once './core/init.php';

if ($admin->isLoggedIn()) {
  Redirect::to('index.php');
}

?>

<header class="py-4 mb-3 shadow-sm bg-white">
  <h3 class="text-center m-auto"><strong>PERPUS</strong></h3>
</header>

<div class="container">
  <div class="row">
    <div class="col-md-5 mx-auto py-3">
      <div class="card ">
        <div class="card-header">
          LOGIN
        </div>
        <form method="POST" class="card-body">

          <?php
          if (Input::exists()) {

            $validate = new Validate();
            $validation = $validate->check($_POST, [
              'username' => array('required' => true),
              'password' => array('required' => true)
            ]);

            if ($validation->passed()) {
              // Login admin
              $admin = new Admin();
              $login = $admin->login(Input::get('username'), Input::get('password'));

              if ($login) {
                Redirect::to('index.php');
              } else {
                echo '<div class="alert alert-danger">Sorry, logging in failed</div>';
              }
            } else {
              echo "<div class='alert alert-danger'><ul class='mb-0'>";
              foreach ($validation->errors() as $error) {
                echo "<li>$error</li>";
              }
              echo "</ul></div>";
            }
          }
          ?>

          <div class="form-group mb-3">
            <label for="username">Username</label>
            <input type="text" id="username" name="username" value="<?= Input::get('username') ?>" class="form-control" />
          </div>
          <div class="form-group mb-3">
            <label for="password">Password</label>
            <input type="password" id="password" name="password" class="form-control" />
          </div>
          <div class="form-group mb-3 text-center">
            <button class="btn btn-primary px-5">LOGIN</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<?php include_once "./includes/bottom.php" ?>