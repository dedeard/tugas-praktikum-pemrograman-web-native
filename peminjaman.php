<?php
require_once './core/init.php';

if (!$admin->isLoggedIn()) {
  Redirect::to('login.php');
}

if (Input::get('delete')) {
  if (!DB::getInstance()->query("DELETE FROM pinjam WHERE id_pinjam=?", [Input::get('delete')])) {
    echo "<div class='alert alert-danger'><div class='container'>Gagal menghapus data</div></div>";
  }
}

include_once "./includes/top.php";
include_once "./includes/nav.php";
?>

<div class="container py-3">
  <div class="card">
    <div class="card-header">Data buku yang dipinjam <a class="btn btn-primary btn-sm float-end" href="buat-peminjaman.php">Tambah data</a></div>
    <table class="table mb-0">
      <tr>
        <th width="30px">ID</th>
        <th>Nama</th>
        <th>No HP</th>
        <th>Judul Buku</th>
        <th>Dipinjam pada</th>
        <th width="80px" class="text-center">Aksi</th>
      </tr>
      <?php
      $sql = "SELECT * FROM pinjam
              INNER JOIN buku ON pinjam.id_buku=buku.id_buku
              INNER JOIN anggota ON pinjam.id_anggota=anggota.id_anggota";
      $data = DB::getInstance()->query($sql)->results();
      foreach ($data as $a) {
        echo "
        <tr>
          <td>$a->id_pinjam</td>
          <td>$a->nama</td>
          <td>$a->no_hp</td>
          <td>$a->judul</td>
          <td>" . date_format(date_create($a->dibuat_pada), 'd M Y') . "</td>
          <td class='text-center'>
            <a class='btn btn-danger btn-sm' href='/peminjaman.php?delete=$a->id_pinjam'>Hapus</a>
          </td>
        </tr>
        ";
      }
      ?>
    </table>
  </div>
</div>

<?php include_once "./includes/bottom.php" ?>