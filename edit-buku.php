<?php
require_once './core/init.php';
if (!$admin->isLoggedIn()) {
  Redirect::to('login.php');
}

include_once "./includes/top.php";

$id = Input::get('id');
$query = DB::getInstance()->query("SELECT * FROM buku WHERE id_buku=?", [Input::get('id')]);
if ($query->count() < 1) {
  Redirect::to(404);
}
$buku = $query->first();

include_once "./includes/nav.php";
?>
<div class="container py-3">
  <div class="card">
    <div class="card-header">Edit buku</div>
    <form method="POST" class="card-body">
      <?php
      if (Input::exists()) {

        $validate = new Validate();

        $validation = $validate->check($_POST, [
          'judul' => ['required' => true, 'min' => 3, 'max' => 30],
          'penulis' => ['required' => true, 'min' => 3, 'max' => 20],
          'rilis' => ['required' => true],
        ]);

        if ($validation->passed()) {
          $judul = Input::get('judul');
          $penulis = Input::get('penulis');
          $rilis_pada = date('Y-m-d H:i:s', strtotime(Input::get('rilis')));
          $sql = "UPDATE buku SET judul=?, penulis=?, rilis_pada=? WHERE id_buku=?";
          if (DB::getInstance()->query($sql, [$judul, $penulis, $rilis_pada, $id])) {
            Redirect::to('buku.php');
          } else {
            echo "<div class='alert alert-danger'>Gagal mengedit buku</div>";
          }
        } else {
          echo "<div class='alert alert-danger'><ul class='mb-0'>";
          foreach ($validation->errors() as $error) {
            echo "<li>$error</li>";
          }
          echo "</ul></div>";
        }
      }
      ?>
      <div class="form-group mb-3">
        <label for="judul">Judul</label>
        <input type="text" id="judul" name="judul" value="<?= $buku->judul ?>" class="form-control" />
      </div>
      <div class="form-group mb-3">
        <label for="penulis">Penulis</label>
        <input type="text" id="penulis" name="penulis" value="<?= $buku->penulis ?>" class="form-control" />
      </div>
      <div class="form-group mb-3">
        <label for="rilis">Tanggal Rilis</label>
        <input type="date" id="rilis" name="rilis" value="<?= date_format(date_create($buku->rilis_pada), 'Y-m-d') ?>" class="form-control" />
      </div>
      <div class="form-group mb-3">
        <button class="btn btn-primary px-5">Edit buku</button>
      </div>
    </form>
  </div>
</div>

<?php include_once "./includes/bottom.php" ?>