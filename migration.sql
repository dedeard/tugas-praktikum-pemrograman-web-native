CREATE DATABASE perpustakaan;

USE perpustakaan;

CREATE TABLE
    `admin` (
        `id_admin` INTEGER NOT NULL AUTO_INCREMENT,
        `username` VARCHAR(191) NOT NULL,
        `nama` VARCHAR(191) NOT NULL,
        `password` VARCHAR(191) NOT NULL,
        UNIQUE INDEX `admin_username_key`(`username`),
        PRIMARY KEY (`id_admin`)
    ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE
    `anggota` (
        `id_anggota` INTEGER NOT NULL AUTO_INCREMENT,
        `no_hp` VARCHAR(191) NOT NULL,
        `nama` VARCHAR(191) NOT NULL,
        PRIMARY KEY (`id_anggota`)
    ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE
    `buku` (
        `id_buku` INTEGER NOT NULL AUTO_INCREMENT,
        `judul` VARCHAR(191) NOT NULL,
        `penulis` VARCHAR(191) NOT NULL,
        `rilis_pada` DATETIME(3) NOT NULL,
        PRIMARY KEY (`id_buku`)
    ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE
    `pinjam` (
        `id_pinjam` INTEGER NOT NULL AUTO_INCREMENT,
        `id_anggota` INTEGER NOT NULL,
        `id_buku` INTEGER NOT NULL,
        `dibuat_pada` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
        PRIMARY KEY (`id_pinjam`)
    ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

ALTER TABLE `pinjam`
ADD
    CONSTRAINT `pinjam_id_buku_fkey` FOREIGN KEY (`id_buku`) REFERENCES `buku`(`id_buku`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `pinjam`
ADD
    CONSTRAINT `pinjam_id_anggota_fkey` FOREIGN KEY (`id_anggota`) REFERENCES `anggota`(`id_anggota`) ON DELETE CASCADE ON UPDATE CASCADE;