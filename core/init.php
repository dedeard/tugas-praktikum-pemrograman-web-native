<?php
session_start();

$GLOBALS['config'] = [
    'mysql' => [
        'host' => 'localhost',
        'username' => 'root',
        'password' => '',
        'db' => 'perpustakaan'
    ],
];

spl_autoload_register(function ($class) {
    require_once 'classes/' . $class . '.php';
});

$admin = new Admin();
