<?php
require_once './core/init.php';
if (!$admin->isLoggedIn()) {
  Redirect::to('login.php');
}

include_once "./includes/top.php";
include_once "./includes/nav.php";
?>
<div class="container py-3">
  <div class="card">
    <div class="card-header">Buat buku</div>
    <form method="POST" class="card-body">
      <?php
      if (Input::exists()) {

        $validate = new Validate();
        $validation = $validate->check($_POST, [
          'judul' => ['required' => true, 'min' => 3, 'max' => 30],
          'penulis' => ['required' => true, 'min' => 3, 'max' => 20],
          'rilis' => ['required' => true],
        ]);

        if ($validation->passed()) {
          $judul = Input::get('judul');
          $penulis = Input::get('penulis');
          $rilis_pada = date('Y-m-d H:i:s', strtotime(Input::get('rilis')));
          $sql = "INSERT INTO buku (judul, penulis, rilis_pada) VALUES (?, ?, ?)";
          if (DB::getInstance()->query($sql, [$judul, $penulis, $rilis_pada])) {
            Redirect::to('buku.php');
          } else {
            echo "<div class='alert alert-danger'>Gagal membuat buku</div>";
          }
        } else {
          echo "<div class='alert alert-danger'><ul class='mb-0'>";
          foreach ($validation->errors() as $error) {
            echo "<li>$error</li>";
          }
          echo "</ul></div>";
        }
      }
      ?>
      <div class="form-group mb-3">
        <label for="judul">Judul</label>
        <input type="text" id="judul" name="judul" value="<?= Input::get('judul') ?>" class="form-control" />
      </div>
      <div class="form-group mb-3">
        <label for="penulis">Penulis</label>
        <input type="text" id="penulis" name="penulis" value="<?= Input::get('penulis') ?>" class="form-control" />
      </div>
      <div class="form-group mb-3">
        <label for="rilis">Tanggal Rilis</label>
        <input type="date" id="rilis" name="rilis" value="<?= Input::get('rilis') ?>" class="form-control" />
      </div>
      <div class="form-group mb-3">
        <button class="btn btn-primary px-5">Buat buku</button>
      </div>
    </form>
  </div>
</div>

<?php include_once "./includes/bottom.php" ?>