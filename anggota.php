<?php
require_once './core/init.php';

if (!$admin->isLoggedIn()) {
  Redirect::to('login.php');
}

if (Input::get('delete')) {
  if (!DB::getInstance()->query("DELETE FROM anggota WHERE id_anggota=?", [Input::get('delete')])) {
    echo "<div class='alert alert-danger'><div class='container'>Gagal menghapus anggota anggota</div></div>";
  }
}

include_once "./includes/top.php";
include_once "./includes/nav.php";

?>

<div class="container py-3">
  <div class="card">
    <div class="card-header">Data anggota <a class="btn btn-primary btn-sm float-end" href="buat-anggota.php">Buat Anggota</a></div>
    <table class="table mb-0">
      <tr>
        <th width="30px">ID</th>
        <th>Nama</th>
        <th>Nomor HP</th>
        <th width="120px" class="text-center">Aksi</th>
      </tr>
      <?php
      $anggota = DB::getInstance()->query("SELECT * from anggota")->results();
      foreach ($anggota as $a) {
        echo "
        <tr>
          <td>$a->id_anggota</td>
          <td>$a->nama</td>
          <td>$a->no_hp</td>
          <td class='text-center'>
            <a class='btn btn-primary btn-sm' href='/edit-anggota.php?id=$a->id_anggota'>Edit</a>
            <a class='btn btn-danger btn-sm' href='/anggota.php?delete=$a->id_anggota'>Hapus</a>
          </td>
        </tr>
        ";
      }
      ?>
    </table>
  </div>
</div>

<?php include_once "./includes/bottom.php" ?>