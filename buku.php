<?php
require_once './core/init.php';

if (!$admin->isLoggedIn()) {
  Redirect::to('login.php');
}

if (Input::get('delete')) {
  if (!DB::getInstance()->query("DELETE FROM buku WHERE id_buku=?", [Input::get('delete')])) {
    echo "<div class='alert alert-danger'><div class='container'>Gagal menghapus buku buku</div></div>";
  }
}

include_once "./includes/top.php";
include_once "./includes/nav.php";

?>

<div class="container py-3">
  <div class="card">
    <div class="card-header">Data buku <a class="btn btn-primary btn-sm float-end" href="buat-buku.php">Buat buku</a></div>
    <table class="table mb-0">
      <tr>
        <th width="30px">ID</th>
        <th>Judul</th>
        <th>Penulis</th>
        <th>Tanggal rilis</th>
        <th width="120px" class="text-center">Aksi</th>
      </tr>
      <?php
      $buku = DB::getInstance()->query("SELECT * from buku")->results();
      foreach ($buku as $a) {
        echo "
        <tr>
          <td>$a->id_buku</td>
          <td>$a->judul</td>
          <td>$a->penulis</td>
          <td>" . date_format(date_create($a->rilis_pada), 'd M Y') . "</td>
          <td class='text-center'>
            <a class='btn btn-primary btn-sm' href='/edit-buku.php?id=$a->id_buku'>Edit</a>
            <a class='btn btn-danger btn-sm' href='/buku.php?delete=$a->id_buku'>Hapus</a>
          </td>
        </tr>
        ";
      }
      ?>
    </table>
  </div>
</div>

<?php include_once "./includes/bottom.php" ?>